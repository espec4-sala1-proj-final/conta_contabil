FROM openjdk:8-jre-alpine

WORKDIR /appagent
COPY /appagent/ ./

COPY target/contaContabil-*.jar api-contaContabil.jar

CMD ["java", "-javaagent:/appagent/javaagent.jar", "-jar", "api-contaContabil.jar"]
