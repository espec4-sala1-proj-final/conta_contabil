package br.com.itau.tarifas.contaContabil.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code= HttpStatus.NOT_FOUND, reason = "Conta Contábil não encontrada")
public class ContaContabilNaoEncontradaException extends RuntimeException {
}