package br.com.itau.tarifas.contaContabil.repositories;

import br.com.itau.tarifas.contaContabil.models.ContaContabil;
import org.springframework.data.repository.CrudRepository;

public interface ContaContabilRepository extends CrudRepository<ContaContabil, Integer> {
}
