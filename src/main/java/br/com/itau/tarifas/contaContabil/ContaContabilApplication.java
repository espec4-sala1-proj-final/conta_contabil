package br.com.itau.tarifas.contaContabil;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ContaContabilApplication {

	public static void main(String[] args) {
		SpringApplication.run(ContaContabilApplication.class, args);
	}

}
