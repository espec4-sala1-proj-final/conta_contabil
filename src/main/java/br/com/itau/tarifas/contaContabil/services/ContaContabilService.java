package br.com.itau.tarifas.contaContabil.services;

import br.com.itau.tarifas.contaContabil.exceptions.ContaContabilNaoEncontradaException;
import br.com.itau.tarifas.contaContabil.models.ContaContabil;
import br.com.itau.tarifas.contaContabil.repositories.ContaContabilRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ContaContabilService {

    @Autowired
    private ContaContabilRepository contaContabilRepository;

    public ContaContabilService(){
    }

    public ContaContabil createContaContabil (ContaContabil contaContabil){
        return contaContabilRepository.save(contaContabil);
    }

    public Iterable<ContaContabil> listarTodasContaContabil(){
        return contaContabilRepository.findAll();
    }


    //Buscar Conta Contábil por ID
    public ContaContabil buscarContaContabilPeloId(int id){
        Optional<ContaContabil> contaContabilOptional = contaContabilRepository.findById(id);

        if(contaContabilOptional.isPresent()){
            ContaContabil contaContabil = contaContabilOptional.get();
            return contaContabil;
        }else {
            System.out.println(System.currentTimeMillis() + " - Erro ao buscar a conta contábil: Conta Contábil não encontrada - " + id);
            throw new ContaContabilNaoEncontradaException();
        }
    }
}