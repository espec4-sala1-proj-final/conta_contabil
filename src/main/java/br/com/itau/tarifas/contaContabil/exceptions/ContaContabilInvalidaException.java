package br.com.itau.tarifas.contaContabil.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.BAD_REQUEST, reason = "Conta Contábil inválida")
public class ContaContabilInvalidaException extends RuntimeException {
}
