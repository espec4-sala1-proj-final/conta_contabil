package br.com.itau.tarifas.contaContabil.controllers;

import br.com.itau.tarifas.contaContabil.exceptions.ContaContabilInvalidaException;
import br.com.itau.tarifas.contaContabil.models.ContaContabil;
import br.com.itau.tarifas.contaContabil.services.ContaContabilService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

@RestController
@RequestMapping("/contabil")
public class ContaContabilController {

    @Autowired
    private ContaContabilService contaContabilService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public ContaContabil createContaContabil(@RequestBody ContaContabil contaContabil){
        try {
            return contaContabilService.createContaContabil(contaContabil);
        } catch (RuntimeException e){
            throw new ContaContabilInvalidaException();
        }
    }

    @GetMapping
    public Iterable<ContaContabil> listarTodasContaContabil(){
        return contaContabilService.listarTodasContaContabil();
    }

    //Consultar Conta Contábil por ID
    @GetMapping("/{id}")
    public ContaContabil pesquisarPorId(@PathVariable(name = "id") int id){
            ContaContabil contaContabil = contaContabilService.buscarContaContabilPeloId(id);
            return contaContabil;
    }
}